using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public GameObject platforms;
    public float speed = 0.05f;
    public GameObject[] segmentsPrefab;
    private int Tick = 0;
    private List<GameObject> _segments = new List<GameObject>();
    private GameObject LastSegment;
    
    public int Score = 0;

    public void Start()
    {
        LastSegment = GameObject.FindGameObjectWithTag("Start");
        _segments.Add(LastSegment);
        for (int i = 0; i < 5; i++)
        {
            GenerateSegment();
        }
    }

    private void GenerateSegment()
    {
        int Index = Random.Range(0, segmentsPrefab.Length);
        GameObject selectedSegment = segmentsPrefab[Index];
        foreach (Transform In in selectedSegment.transform)
        {
            if (In.CompareTag("In"))
            {
                foreach (Transform Out in LastSegment.transform)
                {
                    if (Out.CompareTag("Out"))
                    {
                        GameObject currentSegment = Instantiate(selectedSegment, platforms.transform, true);
                        currentSegment.transform.position = Out.position - In.localPosition;
                        _segments.Add(currentSegment);
                        LastSegment = currentSegment;
                    }
                }
            }
        }
    }

    public void AddScore(int input)
    {
        if (input < 0) return;
        Score += input;
    }

    public void Death()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void FixedUpdate()
    {
        platforms.transform.Translate(Vector2.down * speed);
        if (Tick == 150)
        {
            GenerateSegment();
            Tick = 0;
        }
        else
        {
            Tick++;
        }

        if (_segments.Count>50)
        {
            Destroy(_segments[0]);
            _segments.RemoveAt(0);
        }
    }
}
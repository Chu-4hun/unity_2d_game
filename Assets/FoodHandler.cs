using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodHandler : MonoBehaviour
{
    private GameManager _GameManager;

    void Start()
    {
        _GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _GameManager.AddScore(10);
        Destroy(transform.transform.parent.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
    }
}